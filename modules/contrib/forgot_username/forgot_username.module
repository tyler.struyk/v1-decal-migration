<?php

/**
 * @file
 * Main module file for the forgot_username module.
 */

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function forgot_username_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the forgot_username module.
    case 'help.page.forgot_username':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module allows users who forgot their username retrieve it. Just head to the Forgot Username tab on the login screen, enter your email and voilá. One caveat is that your email might land in the spam folder. The module has nothing to with this.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_mail().
 */
function forgot_username_mail($key, &$message, $params) {
  switch ($key) {
    case 'notice':
      $site_name = \Drupal::config('system.site')->get('name');
      $email = \Drupal::state()->get('forgot_username_mail');
      $params['user'] = user_load_by_mail($email)->getDisplayName();
      $message['subject'] = t('Your username from @site', ['@site' => \Drupal::config('system.site')->get('name')]);
      $message['body'][] = t('You, or someone pretending to be you, requested your username from @site. Your username is @user.', ['@site' => $site_name, '@user' => $params['user']]);
      $message['body'][] = '';
      $message['body'][] = t('Thanks, @site', ['@site' => $site_name]);
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function forgot_username_form_user_admin_settings_alter(&$form, FormStateInterface $form_state, $form_id) {
  $mail_config = \Drupal::service('config.factory')->get('user.mail');
  $config = \Drupal::service('config.factory')->get('user.settings');
  $form['email_forgot_username'] = [
    '#type' => 'details',
    '#title' => t('Username recovery'),
    '#description' => t('Edit the email messages sent to users who request their username.'),
    '#group' => 'email',
    '#weight' => 10,
  ];

  $form['email_forgot_username']['user_mail_forgot_username_activated_notify'] = [
    '#type' => 'checkbox',
    '#title' => t('Notify of forgotten username'),
    '#default_value' => $config->get('notify.username_reset'),
  ];

  $form['email_forgot_username']['user_mail_forgot_username_subject'] = [
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => $mail_config->get('username_reset.subject'),
    '#maxlength' => 180,
  ];

  $form['email_forgot_username']['user_mail_forgot_username_body'] = [
    '#title' => t('Body'),
    '#type' => 'textarea',
    '#default_value' => $mail_config->get('username_reset.body'),
    '#rows' => 12,
  ];

  $form['#submit'][] = 'forgot_username_user_accountsettings_form_submit';
}

/**
 * Submit handler of user account settings form.
 */
function forgot_username_user_accountsettings_form_submit(array $form, FormStateInterface $form_state) {
  $userconfig = \Drupal::getContainer()->get('config.factory')->getEditable('user.settings');
  $userconfig->set('notify.username_reset', $form_state->getValue('user_mail_forgot_username_activated_notify'));
  $userconfig->save();
  // Mail Config.
  $config = \Drupal::getContainer()->get('config.factory')->getEditable('user.mail');
  $config->set('username_reset.body', $form_state->getValue('user_mail_forgot_username_body'));
  $config->set('username_reset.subject', $form_state->getValue('user_mail_forgot_username_subject'));
  $config->save();
}

/**
 * Implements hook_block_view_BASE_ID_alter().
 */
function forgot_username_block_view_user_login_block_alter(array &$build, BlockPluginInterface $block) {
  $build['#pre_render'][] = '_forgot_username_user_login_block_prerender';
}

/**
 * Rendering of the new link.
 */
function _forgot_username_user_login_block_prerender(array $build) {
  $build['content']['forgot_username'] = [
    '#type' => 'link',
    '#title' => t('Forgot username'),
    '#url' => Url::fromRoute('forgot_username.username', [], [
      'attributes' => [
        'title' => t('Forgot username'),
        'class' => ['forgot-username-link'],
      ],
    ]),
  ];
  return $build;
}
