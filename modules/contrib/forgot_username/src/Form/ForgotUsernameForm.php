<?php

namespace Drupal\forgot_username\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for /user/username.
 */
class ForgotUsernameForm extends FormBase {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The email validator service.
   *
   * @var \Eguilas\EmailValidator\EmailValidator
   */
  protected $emailValidator;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database, EmailValidator $emailValidator, MessengerInterface $messenger, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->emailValidator = $emailValidator;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('email.validator'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your email address'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Request username'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'forgot_username.username';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableNames() {
    return 'forgot_username';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email_address');
    $query = $this->database->select('users_field_data', 'u');
    $query->fields('u', ['name']);
    $query->condition('u.mail', $email, '=');
    $user = $query->execute()->fetchAll();
    if (!$this->emailValidator->isValid($email)) {
      $form_state->setErrorByName('email_address', $this->t('The email address is not valid.'));
    }
    if (!$user) {
      $form_state->setErrorByName('email_address', $this->t('There is no account with that email address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email_address');
    $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail'=> $email]);
    $account = reset($users);
    $mail = _user_mail_notify('username_reset', $account);
    if (!empty($mail)) {
      $message = $this->t('Your username has been emailed to the address specified. The email might end up in spam folder, so please check there before complaining.');
      $this->messenger()->addMessage($message);
    }
  }

}
